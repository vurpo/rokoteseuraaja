import asyncio
from aiohttp import ClientSession, web
import json
from jinja2 import Environment, FileSystemLoader
import pandas
import datetime

import fetch

data = None
populations = None
calculateddata = None

def refresh_data():
    global data
    global populations
    global calculateddata
    
    data = fetch.fetch()

    populations = data['populations']

    fetched = datetime.datetime.strptime(data['fetched'], "%Y-%m-%d")

    calculateddata = { key: {} for key in data['data'] }

    # calculate averages and stuff

    for area in data['data']:
        dates = sorted(data['data'][area].keys())
        mostrecentdate = datetime.datetime.strptime(dates[-1], "%Y-%m-%d")
        daterange7 = list(map(lambda date: date.strftime("%Y-%m-%d"), pandas.date_range(end = mostrecentdate, periods = 7).to_list()))
        daterange14 = list(map(lambda date: date.strftime("%Y-%m-%d"), pandas.date_range(end = mostrecentdate, periods = 14).to_list()))

        datapoints = 0.0
        average7 = 0.0
        for date in daterange7:
            if date in data['data'][area]:
                datapoints += 1.0
                average7 += float(data['data'][area][date])
        average7 /= datapoints
        calculateddata[area]['tahti7'] = average7

        datapoints = 0.0
        average14 = 0.0
        for date in daterange14:
            if date in data['data'][area]:
                datapoints += 1.0
                average14 += float(data['data'][area][date])
        average14 /= datapoints
        calculateddata[area]['tahti14'] = average14

        calculateddata[area]['määrä'] = int(data['total'][area])

        try:
            calculateddata[area]['osuus'] = float(data['total'][area])/populations[area]

            targetamount = populations[area]*0.7
            togo = targetamount-int(data['total'][area])
            
            if average7 > 0:
                calculateddata[area]['arvioitu7'] = (fetched+datetime.timedelta(days=togo/average7)).strftime("%Y-%m-%d")
            else:
                calculateddata[area]['arvioitu7'] = '-'
            
            if average14 > 0:
                calculateddata[area]['arvioitu14'] = (fetched+datetime.timedelta(days=togo/average14)).strftime("%Y-%m-%d")
            else:
                calculateddata[area]['arvioitu14'] = '-'
            calculateddata[area]['väkiluku'] = populations[area]
        except KeyError:
            calculateddata[area]['osuus'] = None
            calculateddata[area]['arvioitu7'] = None
            calculateddata[area]['arvioitu14'] = None
            calculateddata[area]['väkiluku'] = None

refresh_data()

async def refresh_job():
    while True:
        tomorrow = (datetime.datetime.today() + datetime.timedelta(days=1)).replace(hour=15, minute=0, second=0)
        now = datetime.datetime.now()
        print(f"sleeping until {tomorrow}")
        await asyncio.sleep((tomorrow-now).total_seconds())
        refresh_data()

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)
root_template = env.get_template("root.html")

routes = web.RouteTableDef()

@routes.get('/{lang}')
async def page(request):
    if request.match_info['lang'] in ['fi', 'sv', 'en']:
        lang = request.match_info['lang']
        return web.Response(
            content_type='text/html',
            text=root_template.render(
                lang = lang,
                aluenimet = data['areas'][lang],
                alueet = calculateddata,
                haettu = data['fetched']
            )
        )
    else:
        return web.Response(status=404)

@routes.get('/')
async def root(request):
    return web.HTTPFound('/fi')

async def main():
    asyncio.create_task(refresh_job())
    app = web.Application()
    app.add_routes(routes)
    return app

web.run_app(main())
