import requests
import pprint
import json
import datetime

pp = pprint.PrettyPrinter(indent=2)

headers={'User-Agent':'aaa'}

def fetch():
    weeksdata_fi = requests.get('https://sampo.thl.fi/pivot/prod/fi/vaccreg/cov19cov/fact_cov19cov.json?row=area-518362&column=cov_vac_dose-533170', headers=headers).json()
    weeksdata_sv = requests.get('https://sampo.thl.fi/pivot/prod/sv/vaccreg/cov19cov/fact_cov19cov.json?row=area-518362&column=cov_vac_dose-533170', headers=headers).json()
    weeksdata_en = requests.get('https://sampo.thl.fi/pivot/prod/en/vaccreg/cov19cov/fact_cov19cov.json?row=area-518362&column=cov_vac_dose-533170', headers=headers).json()

    areas = {}
    areas['fi'] = weeksdata_fi['dataset']['dimension']['area']['category']['label']
    areas['sv'] = weeksdata_sv['dataset']['dimension']['area']['category']['label']
    areas['en'] = weeksdata_en['dataset']['dimension']['area']['category']['label']

    areaindices = weeksdata_fi['dataset']['dimension']['area']['category']['index']

    totalamounts = { key: {} for key in areas }

    for area in areaindices:
        totalamounts[area] = weeksdata_fi['dataset']['value'][str(areaindices[area]*weeksdata_fi['dataset']['dimension']['size'][1])]

    dailydata = { key: {} for key in areaindices } # create dict of all areas -> empty dicts to put daily data in

    weekdata = requests.get(f'https://sampo.thl.fi/pivot/prod/fi/vaccreg/cov19cov/fact_cov19cov.json?row=518362&column=533037L&column=cov_vac_dose-533170', headers=headers).json()
    days = weekdata['dataset']['dimension']['dateweek20201226']['category']
    dayindices = [(days['label'][day[0]], days['index'][day[0]]) for day in sorted(days['label'].items(), key=lambda x: x[1])]

    areaindices = weekdata['dataset']['dimension']['area']['category']['index']
    rowlength = weekdata['dataset']['dimension']['size'][1]

    for area in areaindices:
        startindex = areaindices[area]*rowlength
        for day in dayindices:
            index = startindex + day[1]
            if str(index) in weekdata['dataset']['value']:
                dailydata[area][day[0]] = weekdata['dataset']['value'][str(index)]

    populationsdata = requests.get('https://sampo.thl.fi/pivot/prod/fi/vaccreg/cov19cov/fact_cov19cov.json?row=area-518362&column=measure-433796', headers=headers).json()
    populations = {
        area: populationsdata['dataset']['value'].get(str(populationsdata['dataset']['dimension']['area']['category']['index'][area]), None)
        for area in populationsdata['dataset']['dimension']['area']['category']['index'].keys()
    }
    populations = { area: int(population) for (area, population) in populations.items() if population != None }

    return {
            "areas": areas,
            "data": dailydata,
            "total": totalamounts,
            "populations": populations,
            "fetched": datetime.datetime.now().strftime("%Y-%m-%d")
        }

if __name__ == '__main__':
    result = fetch()
    with open('data.json', 'w', encoding='utf-8') as file:
       json.dump(result, file)