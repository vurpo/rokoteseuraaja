FROM python:3.7-slim-buster

RUN apt-get update -y
RUN apt-get install -y build-essential python-dev

COPY requirements.txt /opt/rokoteseuraaja/requirements.txt

RUN pip3 install -r /opt/rokoteseuraaja/requirements.txt

COPY templates /opt/rokoteseuraaja/templates
COPY fetch.py /opt/rokoteseuraaja/
COPY serve.py /opt/rokoteseuraaja/

WORKDIR /opt/rokoteseuraaja

CMD python3 serve.py